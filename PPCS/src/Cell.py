class Cell:
    
    '''
    Class representing a grid in the worldview.
    '''
    
    def __init__(self, identifier, x1, x2, y1, y2, channel_activity):
        '''
        Constructor
        '''
        self.cell_id = identifier
        self.x_min = x1
        self.x_max = x2
        self.y_min = y1
        self.y_max = y2
        self.channel_info = channel_activity
        self.users_num = 0
    
    # getters and setters
    
    def get_cell_id(self):
        return self.cell_id
    
    def set_cell_id(self, new_id):
        self.cell_id = new_id
    
    def get_x_min(self):
        return self.x_min
    
    def set_x_min(self, new_x_min):
        self.x_min = new_x_min
        
    def get_x_max(self):
        return self.x_max
    
    def set_x_max(self, new_x_max):
        self.x_max = new_x_max
    
    def get_y_min(self):
        return self.y_min
    
    def set_y_min(self, new_y_min):
        self.y_min = new_y_min
        
    def get_y_max(self):
        return self.y_max
    
    def set_y_max(self, new_y_max):
        self.y_max = new_y_max
        
    def get_channel_info(self):
        return self.channel_info
    
    def set_channel_info(self, new_channel_info):
        self.channel_info = new_channel_info 
        
    def get_users_num(self):
        return self.channel_info
    
    def set_users_num(self, new_users_num):
        self.users_num = new_users_num
    
    def is_in_cell(self, point):
        point_x = point[0]
        point_y = point[1]
        
        if(point_x >= self.x_min and point_x <= self.x_max):
            if(point_y >= self.y_min and point_y <= self.y_max):
                return True
            return False
        return False
