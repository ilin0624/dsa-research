class Report:

    def __init__(self, cell_id, channel_id, availability, time):
        self.channel_id = channel_id
        self.channel_availability = availability
        self.time_generated = time

        @property
        def time_generated(self):
            return time_generated

        @time_generated.setter
        def time_generated(self, report_time):
            self.time_generated = report_time
    

        @property
        def channel_availability(self):
            return channel_availability

        @channel_availability.setter
        def channel_availability(self, availability_status):
            self.channel_availability = availability_status


    def generate_report(self):
        pass