class Worker:
    
    '''
    Class representing a user in the privacy-preserving crowd sensing scheme.
    '''

    def __init__(self, id, trajectory):
        '''
        Constructor
        
        -worker_id
        -trajectory
        -visited
        -channel_info
        
        '''
        self.worker_id = id
        self.trajectory = trajectory
        self.visited_cells = {}
        self.sensed = {} # only the querier will use this variable, which notes if they have already queried a planned future cell

    def get_worker_id(self):
        return self.worker_id

    def set_worker_id(self, new_worker_id):
        self.worker_id = new_worker_id

    def get_trajectory(self):            
        return self.trajectory

    def set_trajectory(self, coordinates):
        self.trajectory = coordinates

    def get_visited_cells(self):
        return self.visited_cells

    def set_visited_cells(self, visited):
        self.visited_cells = visited
        
    def get_sensed(self):
        return self.sensed

    def set_sensed(self, sense):
        self.sensed = sense
        
    def query_prep(self, keys):
        prep = {}
        for x in range(0, len(keys)):
            prep[keys[x]] = False
        self.set_sensed(prep)
        

        