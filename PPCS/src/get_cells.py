import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import itertools


def parse_taxi_data(filepath):
    '''
    Parses .txt file of taxi coordinates, where each line is in the format:
    taxi id, date time, longitude, latitude.

    Parameters
    ----------
    filepath : str
        file path of the .txt file being parsed

    Returns
    -------
    list, list
        list of coordinate pairs, the trajectory's bounds
    '''

    longitude = []
    latitude = []
    coordinates = []
    boundaries = []

    data = pd.read_csv(filepath, names=['id', 'date and time', 'longitude', 'latitude'])

    for x in data['longitude']:
        longitude.append(x)
    for y in data['latitude']:
        latitude.append(y)

    x_min = min(longitude)
    x_max = max(longitude)
    y_min = min(latitude)
    y_max = max(latitude)

    boundaries.append(x_min)
    boundaries.append(x_max)
    boundaries.append(y_min)
    boundaries.append(y_max)
    # boundaries = [x_min, x_max, y_min, y_max]

    for z in range(0, len(longitude)):
        coordinates.append([longitude[z], latitude[z]])

    coordinates_df = pd.DataFrame(coordinates, columns=['long', 'lat'])
    return coordinates_df, boundaries


def parse_geolife_data(filepath):
    '''
    Parses .txt file of a user's coordinates, where each line is in the format:
    latitude,longitude,0,altitude,days,date,time

    Parameters
    ----------
    filepath : str
        file path of the .txt file being parsed

    Returns
    -------
    list, list
        list of coordinate pairs, the trajectory's bounds
    '''
    longitude = []
    latitude = []
    coordinates = []
    boundaries = []

    data = pd.read_csv(filepath, names=['latitude', 'longitude', '0', 'altitude', 'days', 'date', 'time'])

    for x in data['longitude']:
        longitude.append(x)
    for y in data['latitude']:
        latitude.append(y)

    x_min = min(longitude)
    x_max = max(longitude)
    y_min = min(latitude)
    y_max = max(latitude)

    boundaries.append(x_min)
    boundaries.append(x_max)
    boundaries.append(y_min)
    boundaries.append(y_max)
    # boundaries = [x_min, x_max, y_min, y_max]

    for z in range(0, len(longitude)):
        coordinates.append([longitude[z], latitude[z]])

    return coordinates, boundaries


def plot(coordinates, x1, x2, y1, y2, ax=None):
    '''
    Uses matplotlib to plot a given set of coordinates on a 2-D axis.

    Parameters
    ----------
    coordinates : list
        coordinates to plot
    x1 : double
        minimum longitude of worldview
    x2 : double
        maximum longitude of worldview
    y1 : double
        minimum latitude of worldview
    y2 : double
        maximum latitude of worldview
    '''
    # 100m distance is roughly .0009 degrees of latitude
    # 100m distance is roughly .00117 degrees of longitude

    plt.xlim(x1, x2)
    plt.ylim(y1, y2)
    plt.xticks(np.arange(x1, x2, step=0.00117))
    plt.yticks(np.arange(y1, y2, step=0.0009))
    plt.xlabel('longitude')
    plt.ylabel('latitude')
    plt.grid(True)

    to_plot = np.array(coordinates)
    plt.scatter(to_plot[:, 0], to_plot[:, 1])
    plt.plot(to_plot[:, 0], to_plot[:, 1], linewidth=1.0)

    plt.show()



def main():
    #taxi_trajectory = 'taxi1.txt'
    #taxi_coordinates, t_bounds = parse_taxi_data(taxi_trajectory)
    geo_trajectory = 'geolife20081023025304.plt'
    geo_coordinates, g_bounds = parse_geolife_data(geo_trajectory)
    #x_min = t_bounds[0]
    #x_max = t_bounds[1]
    #y_min = t_bounds[2]
    #y_max = t_bounds[3]


    x_min = g_bounds[0]
    x_max = g_bounds[1]
    y_min = g_bounds[2]
    y_max = g_bounds[3]


    plot(geo_coordinates, x_min, x_max, y_min, y_max)
    #plot(taxi_coordinates, x_min, x_max, y_min, y_max)


if __name__ == "__main__":
    main()
